﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nettverk;

namespace Test_Chat
{
    public partial class Form1 : Form
    {
        ServerSocket server;
        NettverkSocket socket;
        bool AmHost = false;
        public Form1()
        {
            InitializeComponent();
            textBox2.Text = "test";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            server = new ServerSocket();
            button2.Enabled = false;
            var alwayslistenthread = new Thread(
                () => server.OpenServer(8113, 8114));
            alwayslistenthread.IsBackground = true;
            alwayslistenthread.Start();
            server.Send("test", "00");
            AmHost = true;
            //now that we have a connection and it is launched
            Thread ReceiveThread = new Thread(new ThreadStart(chatReceive));
            ReceiveThread.IsBackground = true;
            ReceiveThread.Name = "myServerRecThread";
            ReceiveThread.Start();
        }
        private void chatReceive()
        {
            while (true)
            {
                if (AmHost == false)
                {
                    if (socket.IsSending() == false)
                    {
                        string msg = socket.Receive();
                        if (msg != "")
                        {
                            int type = socket.getmsgtype(msg);
                            msg = msg.Remove(0, 5);
                            HandleMessage(type, msg);
                        }
                    }
                    if (socket.Killing == true)
                    {
                        //terminate
                        socket.ThreadClosed = true;
                        Thread.CurrentThread.Abort();
                    }
                }
                else
                {
                    if (server.IsSending() == false & server.noClients != 0)
                    {
                        string[] msgs = server.Receive();
                        if (msgs[0] != "")
                        {
                            int type = server.getmsgtype(msgs[0]);
                            string msg = msgs[0].Remove(0, 5);
                            HandleMessage(type, msg);
                        }
                    }
                }
            }
        }

        private void HandleMessage(int type, string msg)
        {
            switch(type)
            {
                case 0:
                    break;//do nothing
                case 1:
                    this.Invoke(() => AppendText(ref richTextBox1, msg, Color.Green));
                    break;
                case 2:
                    string unmasked = Leyna.Leyna.UndoLeyna(msg, textBox2.Text);
                    this.Invoke(() => AppendText(ref richTextBox1, unmasked, Color.Green));
                    break;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (AmHost == false)
            {
                //string masked = Leyna.Leyna.PerformLeyna(richTextBox2.Text, textBox2.Text + "/" + richTextBox2.Text.Length.ToString(), false);
                socket.Send(richTextBox2.Text, "01");
            }
            else
            {
                server.Send(richTextBox2.Text, "01");
            }
            AppendText(ref richTextBox1, richTextBox2.Text, Color.Blue);
            richTextBox2.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            socket = new NettverkSocket(false);
            button1.Enabled = false;
            socket.RunClient(textBox1.Text, 8113);
            //now that we have connected and said hello let us get the receiving thread going
            socket.disconnect();

            socket = new NettverkSocket(false);
            socket.RunClient(textBox1.Text, 8114);
            Thread myClientReceiveThread = new Thread(new ThreadStart(chatReceive))
            {
                IsBackground = true,
                Name = "myClientReceiveThread"
            };
            myClientReceiveThread.Start();
        }
        private static void AppendText(ref RichTextBox box, string text, Color color)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;

            box.SelectionColor = color;
            box.AppendText(text + Environment.NewLine);
            box.SelectionColor = box.ForeColor;
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
    public static class ControlExtensions
    {
        public static void Invoke(this Control Control, Action Action)
        {
            Control.Invoke(Action);
        }
    }
}
