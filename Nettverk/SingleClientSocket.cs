﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Nettverk
{
    public class NettverkSocket
    {
        bool Server;
        public bool Killing = false;
        public bool ThreadClosed = false;
        NettverkServer.SocketServer myServer = new NettverkServer.SocketServer();
        NettverkClient.SocketClient myClient = new NettverkClient.SocketClient();
        public string connectedIP;
        public NettverkSocket(bool intent)
        {
            Server = intent;
        }
        public void disconnect()
        {
            if (Server == true)
                myServer.ShutdownServer();
            else
                myClient.ShutdownClient();
        }
        public bool IsSending()
        {
            if (Server == true)
                return myServer.IsSending();
            else
                return myClient.IsSending();
        }
        public void RunServer(string ipthatwillconnect, int port)
        {
            myServer.RunServer(ipthatwillconnect, port);
            connectedIP = myServer.connectedIP;
        }
        public void RunClient(string ip, int port)
        {
            myClient.RunClient(ip, port);
            connectedIP = myClient.connectedIP;
        }
        public void Send(string msg, string type)
        {
            if (Server == true)
                myServer.Send(msg, type);
            else
                myClient.Send(msg, type);
        }

        public int getmsgtype(string msg)
        {
            string strype = msg.Remove(2, msg.Length - 2);
            return Convert.ToInt32(strype);
        }

        public string Receive()
        {
            if (Server == true)
                return RemoveEOF(Encoding.ASCII.GetString(myServer.Receive()));
            else
                return RemoveEOF(myClient.Receive());
        }
        private string RemoveEOF(string input)
        {
            if (input != "")
                return input.Remove(input.Length - 5, 5);
            else return input;
        }
    }
}