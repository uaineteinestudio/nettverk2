﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace NettverkServer
{
    public class SocketServer
    {
        // Data buffer for incoming data. 
        public const int bufferLength = 1024;
        private byte[] bytes = new byte[bufferLength];
        private string data = null;

        private Socket listener;
        private Socket handler;
        private IPEndPoint localEndPoint;

        private bool Sending = false;

        public string connectedIP = "";
        string iptoconnectto;
        public bool IsSending()
        {
            return Sending;
        }

        private int ReceiveErrorCount = 0;

        public void RunServer(string ipthatwillconnect, int port)
        {
            // Establish the local endpoint for the socket.  
            // Dns.GetHostName returns the name of the   
            // host running the application.  
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            //IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPAddress ipAddress = IPAddress.Parse("0.0.0.0");
            localEndPoint = new IPEndPoint(ipAddress, port);

            // Create a TCP/IP socket.  
            listener = new Socket(ipAddress.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);
            iptoconnectto = ipthatwillconnect;
            Listen();//will pause here until accepted connection is made
        }

        private void Listen()
        {
            try
            {
                while (true)
                {
                    listener.Bind(localEndPoint);
                    listener.Listen(10);
                    Console.WriteLine("Waiting for a connection...");
                    // Program is suspended while waiting for an incoming connection.  
                    handler = listener.Accept();
                    IPEndPoint remoteendpoint = handler.RemoteEndPoint as IPEndPoint;
                    connectedIP = remoteendpoint.Address.ToString();
                    if (iptoconnectto != "any")
                    {
                        if (iptoconnectto != connectedIP)
                        {
                            continue;
                        }
                    }

                    Console.WriteLine(connectedIP);

                    //ReceiveAndEcho();
                    break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
        private void ReceiveAndEcho()
        {
            byte[] rec = Receive();
            Send(rec);
        }
        public byte[] Receive()
        {
            data = null;
            Console.WriteLine("Waiting for data...");// An incoming connection needs to be processed.  
            while (true)
            {
                try
                {
                    bytes = new byte[bufferLength];
                    int bytesRec = 0;//handler.Receive(bytes);
                    handler.ReceiveTimeout = 6000;
                    bytesRec = handler.Receive(bytes);

                    data += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                    if (data.IndexOf("<EOF>") > -1)
                    {
                        break;
                    }
                    //reset count
                    ReceiveErrorCount = 0;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    ReceiveErrorCount += 1;
                    if (ReceiveErrorCount > 500)
                    {
                        //kill thread
                    }
                }
            }

            // Echo the data back to the client.  
            byte[] msg = { };
            if (data != null)
                msg = Encoding.ASCII.GetBytes(data);
            //now obliterate data
            data = null;
            return msg;
        }
        public void Send(byte[] message)
        {
            Sending = true;
            handler.Send(message);
            Console.WriteLine("Server has sent");
            Sending = false;
        }
        public void Send(string message, string type)
        {
            Sending = true;
            byte[] msg = Encoding.ASCII.GetBytes(type+"<T>"+message + "<EOF>");
            handler.Send(msg);
            Sending = false;
            Console.WriteLine("Server has sent");
        }
        public void ShutdownServer()
        {
            handler.Shutdown(SocketShutdown.Both);
            handler.Close();
        }
    }
}